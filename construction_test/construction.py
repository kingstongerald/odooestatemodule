import time
#from odoo import models, fields, api, _
#from odoo.exceptions import except_orm, ValidationError

#from odoo.tools import misc, DEFAULT_SERVER_DATETIME_FORMAT
#from dateutil.relativedelta import relativedelta
#import datetime
#from datetime import datetime, timedelta
#from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT


import time
from odoo import models, fields, api, _
from odoo.exceptions import except_orm, ValidationError
from odoo.tools import misc, DEFAULT_SERVER_DATETIME_FORMAT
from dateutil.relativedelta import relativedelta
import datetime

class Account_invoice(models.Model):
    _inherit = "account.invoice"

    '''@api.multi
    def name_get(self):
        result = []
        for record in self:
            namey = record.number
            if record.amount_total:
                names = "[INV:" +str(record.number)+"] [TOTAL:" + str(record.amount_total)+ "] [DATE:" +str(record.date_invoice)
                result.append((record.id, names))
        return result
    '''

    @api.multi
    def name_get(self):
        if not self.ids:
            return []
        res=[]
        for field6 in self.browse(self.ids):
            name = field6.number
            nam = "[Name: "+str(name)
            res.append((field6.id, nam +"]    Date: ["+str(field6.date_invoice)+"]"))
        return res


class Account_Abstract_Payment(models.Model):
    _inherit = "account.payment"
    get_line = fields.Many2one('plot.allocatex', 'Plot Allocation ID')

    @api.multi
    def post(self):
        """ Create the journal items for the payment and update the payment's state to 'posted'.
            A journal entry is created containing an item in the source liquidity account (selected journal's default_debit or default_credit)
            and another in the destination reconciliable account (see _compute_destination_account_id).
            If invoice_ids is not empty, there will be one reconciliable move line per invoice to reconcile with.
            If the payment is a transfer, a second journal entry is created in the destination journal to receive money from the transfer account.
        """
        for rec in self:

            if rec.state != 'draft':
                raise UserError(_("Only a draft payment can be posted. Trying to post a payment in state %s.") % rec.state)

            if any(inv.state != 'open' for inv in rec.invoice_ids):
                raise ValidationError(_("The payment cannot be processed because the invoice is not open!"))

            # Use the right sequence to set the name
            if rec.payment_type == 'transfer':
                sequence_code = 'account.payment.transfer'
            else:
                if rec.partner_type == 'customer':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.customer.invoice'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.customer.refund'
                if rec.partner_type == 'supplier':
                    if rec.payment_type == 'inbound':
                        sequence_code = 'account.payment.supplier.refund'
                    if rec.payment_type == 'outbound':
                        sequence_code = 'account.payment.supplier.invoice'
            rec.name = self.env['ir.sequence'].with_context(ir_sequence_date=rec.payment_date).next_by_code(sequence_code)

            # Create the journal entry
            amount = rec.amount * (rec.payment_type in ('outbound', 'transfer') and 1 or -1)
            move = rec._create_payment_entry(amount)

            # In case of a transfer, the first journal entry created debited the source liquidity account and credited
            # the transfer account. Now we debit the transfer account and credit the destination liquidity account.
            if rec.payment_type == 'transfer':
                transfer_credit_aml = move.line_ids.filtered(lambda r: r.account_id == rec.company_id.transfer_account_id)
                transfer_debit_aml = rec._create_transfer_entry(amount)
                (transfer_credit_aml + transfer_debit_aml).reconcile()



            rec.write({'state': 'posted', 'move_name': move.name})

            get_partner = self.env['plot.allocatex'].search([('so_no', '=', rec.communication)])
            values = {
            'journal_id': rec.journal_id.id,
            'payment_method_id': rec.payment_method_id.id,
            'payment_date': rec.payment_date,
            'communication': rec.communication,
            #'invoice_ids': [(4, inv.id, None) for inv in self._get_invoices()],
            'payment_type': rec.payment_type,
            'amount': rec.amount,
            'currency_id': rec.currency_id.id,
            'partner_id': rec.partner_id.id,
            'partner_type': rec.partner_type,
        }

            get_partner.write({'payment_breakdown_invoice':[(0,0,(values))]})
    def get_all_payment(self):
        for rec in self:
            payments_search = rec.env['plot.allocatex'].search([('so_no', '=', self.communication)])

            rec.payment_breakdown_invoice = self.ids

        return True

    '''@api.multi
    def create_payment(self):
        payment = self.env['account.payment'].create(self.get_payment_vals()) #payment_breakdown_invoice

        send_payment = self.env['plot.allocatex'].search([('partner_id','=', self.partner_id.id)])
        #send_payment = self.env['plot.allocatex'].search([('so_no','=', self.communication.id)])
        post_payment = send_payment.payment_breakdown_invoice.create(payment)
        payment.post()
        return {'type': 'ir.actions.act_window_close'}'''

class Product_Custom(models.Model):
    _inherit = "product.template"
    occupied_product = fields.Boolean('Occupied', default = False)



class plot_allocate(models.Model):
    _name = 'plot.allocatex'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    def _compute_ref(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['unit.masterx'].browse(active_ids)
        return expense_sheet.building_type.name
    def _compute_ref_two(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['unit.masterx'].browse(active_ids)
        return expense_sheet.phase.name
    def _compute_ref_three(self):
        context = dict(self._context or {})
        active_ids = context.get('active_ids', [])
        expense_sheet = self.env['unit.masterx'].browse(active_ids)
        return expense_sheet.house_type.name


    """
    @api.constrains('plot_no')
    def restrict_mode(self):
        if self.plot_no.occupied_product == True:
            raise ValidationError('You cannot select two Plots allocated to another customer')
        return True
    """
    def _get_invoices(self):
        return self.env['account.invoice'].search([], limit =1)

    @api.depends('payment_invoice')
    def _get_auto_field_product(self):

        for order in self:
            if order.payment_invoice:
                get_so = self.env['sale.order'].search([('name', '=',order.payment_invoice.origin)])
                get_product = get_so.product_id
                buil_type = get_so.product_id.categ_id.id#.name
                offer_pricex = get_so.product_id.lst_price
                order.update({'plot_no': get_product, 'build_type':buil_type, 'project_type':buil_type, 'offer_price':offer_pricex})
            #return True

    @api.depends('payment_invoice')
    def _get_invoice_fields(self):
        for rec in self:
            for rex in rec.payment_invoice:
                if rec.payment_invoice[0]:

                    partner_idx = rex.partner_id
                    #offer_pricex = rex.amount_total
                    offer_date = rex.date_invoice
                    ref_num = rex.number
                    payment_term = rex.payment_term_id
                    origin = rex.origin

                    rec.update({
                                'partner_id':partner_idx,
                                'reference_number':ref_num,
                                'payment_plan':payment_term,
                                'offer_dt': offer_date,
                                #'offer_price':offer_pricex,
                                'so_no':origin
                                })
    @api.constrains('plot_no')
    def Rex_change(self):
        get_plot_no = self.env['product.product'].search([('id','=',self.plot_no.id)])
        #get_plot_no.write({'occupied_product':True})
        if get_plot_no.occupied_product == True:
            raise ValidationError('The plot has been allocated before!')

    '''@api.multi
    def name_get(self):
        result = []
        for record in self:
            namey = record.reference_number
            if record.amount_dt:
                names = "PLOT REF:" +str(record.reference_number)+"] [TOTAL:" + str(record.amount_dt)+ "] [DATE:" +str(record.offer_dt)
                result.append((record.id, names))
        return result'''
    def name_get(self):
        if not self.ids:
            return []
        res=[]
        for field6 in self.browse(self.ids):
            name = field6.plot_no.name
            nam = "Name: "+str(name)
            res.append((field6.id, nam +"]    REF: ["+str(field6.reference_number)+"]"))
        return res


    invoice_id = fields.Many2one('account.invoice', string = 'Invoice',domain=[('state', '=', 'open')])#,compute="_get_auto_field")#, default = _get_invoices)
    plot_no = fields.Many2one('product.product','Plot Number', default=_get_auto_field_product)
    project_type = fields.Many2one('product.category', string='Project',compute=_get_auto_field_product, store = True)#, domain = [('status','=', 'unallocated')])
    build_type = fields.Many2one('product.category', string='Type of building',compute=_get_auto_field_product, store = True)#, compute= 'onchange_plot_no',required= True, store = True)#related = )
    reference_number = fields.Char('Ref. Number', compute ="_get_invoice_fields",store=True)
    partner_id = fields.Many2one('res.partner', 'Customer', compute ="_get_invoice_fields", required = True)
    offer_dt = fields.Date('Offer Date',compute ="_get_invoice_fields")#, default = fields.Date.today)
    deallocation_date = fields.Date('Deallocation date')#,default=lambda *a: (datetime.datetime.now() + relativedelta(days=7)).strftime('%Y-%m-%d %H:%M:%S'))
    ##offer_price = fields.Float('Offer Price')#, compute= 'onchange_plot_no',)
    offer_price = fields.Float(string='Offer Amount',store=True, readonly=True, compute ="_get_auto_field_product")#compute="_offer_price")
    #company_currency_id = fields.Many2one('res.currency', related='invoice_id.company_currency_id', readonly=True, related_sudo=False)
    payment_type = fields.Selection([('installment', 'Installment'),
            ('outright', 'OutRight Payment')],
            "Payment Type")
    so_no = fields.Char('Sale order No.',store=True, readonly=True,compute ="_get_invoice_fields")
    balance = fields.Float(string ='Balance', store = True, readonly= True,compute='_balancing', track_visibility='always')
    amount_dt = fields.Float(string='Amount Paid')
    total_payment = fields.Float(string="Total Payments", compute = "_get_all_invoice_payment")
    agent = fields.Many2one('res.partner',  string='Agent')#domain=[('agent', '=', True)], string='Agent')
    state = fields.Selection([('draft', 'Draft'),('first', 'Approved'),('reserved', 'Reserved'),
            ('deallocation', 'Deallocated'),
            ('allocation', 'Allocated')],'Status', default='draft')

    active = fields.Boolean('Active', default = True)

    payment_plan = fields.Many2one('account.payment.term', 'Payment Terms', compute ="_get_invoice_fields")
    payment_breakdown = fields.One2many('plot.payment.breakdownx', 'project_and_plot_id', 'Payment Breakdown')
    #payment_breakdown_invoice = fields.One2many('account.payment', 'get_line',string='Breakdowns')
    payment_breakdown_invoice = fields.Many2many('account.payment',string='Breakdowns')# 'get_line',
    description = fields.Text('Description')
    payment_invoice = fields.Many2one('account.invoice', 'Invoice ID')


################################
    name = fields.Many2one('projectsite.masterx','Project Site')

    building = fields.Char(string='House type')
    phase = fields.Char(string='Phase')
    offer_letter_reference_number = fields.Char('Offer Letter Ref Number')
    count_total_payment = fields.Float(string = 'Total Payment')
    saleorder_exists = fields.Boolean(string='Saleorder')
    payment_form_id = fields.Many2one('register.payment.view.for.plotx', 'Payment')
##################################

    '''@api.depends('invoice_id')
    def _get_default_invoice_line(self):
        returnr = self.env['account.invoice'].search([('id', '=', self.invoice_id.id)], limit=1)
        get_return = returnr.invoice_line_ids
        return get_return'''


    @api.multi
    def get_all_payment(self):
        for rec in self:
            payments_search = rec.env['account.payment'].search([('partner_id', '=', self.partner_id.id)])
            #(['|',('partner_id', '=', self.partner_id)])
            rec.payment_breakdown_invoice = payments_search
            amount_balance = rec.offer_price - rec.total_payment
            rec.balance = amount_balance
            #rec.update({'balance': rec.offer_price - rec.total_payment})
        return True

    @api.multi
    def payment_all_view(self):
        return self.open_payment_form()
    @api.multi
    def open_payment_form(self):

        return {
                'type':'ir.actions.act_window',
                'res_model':'account.payment',
                'res_id':self.id,
                'view_type':'tree',
                'view_mode':'tree',
                'target':'new',
                'domain': [('partner_id','=', self.partner_id.id)]
    }





    @api.depends('payment_breakdown_invoice.amount', 'offer_price')
    def _balancing(self):
        for rec in self:
            amount_balance = 0.0
            for line in rec.payment_breakdown_invoice:
                amount_balance += line.amount
            rec.update({'balance': rec.offer_price - amount_balance})


    @api.depends('payment_breakdown_invoice')
    def _get_all_invoice_payment(self):
        #self.ensure_one()
        for order in self:
            amount_totalx = 0.0
            for line in order.payment_breakdown_invoice:
                #balance=line.price_subtotal
                if order.payment_breakdown_invoice[0]:
                    amount_totalx += line.amount
            order.update({'total_payment': amount_totalx})#, 'balance':balance})
#track_visibility='always'


    @api.multi
    def button_first(self):
        for plot in self:
            self.write({'state':'first'})
            plot_no = plot.plot_no.name
            body ="New offer for plot %s awaiting approval" %plot_no
            get_plot_no = self.env['product.product'].search([('id','=',self.plot_no.id)])
            get_plot_no.write({'occupied_product':True})
            #occupied_state = get_plot_no.occupied_product
            #self.write({'occupied_state': True})
            self.message_post(body=body)

    @api.multi
    def button_allocation(self):
        for plot in self:
            self.write({'state':'allocation'})
            plot_no = plot.plot_no.name
            body ="Plot %s was approved" %plot_no
            self.message_post(body=body)

# contruct_report.bh_city_offer_letter_report_document  = DETACHED
# contruct_report.galadimawa_carcas_offer_letter_report_document
# contruct_report.fish_letter_report_document_main
# contruct_report.report_offer_letter_new
    '''@api.multi
    def print_final_allocation_letter(self):
        body = "%s Printed an offer letter" %self.env.user.name
        self.message_post(body=body)
        report = self.env["ir.actions.report.xml"].search([('report_name','=','contruct_report.fish_letter_report_document_main')],limit=1)
        if report:
            report.write({'report_type':'qweb-pdf'}) #contruct_report.allocation_document_offer_one
        return self.env['report'].get_action(self.id, 'contruct_report.fish_letter_report_document_main')'''
    @api.multi
    def print_final_allocation_letter(self):

        plot_namex = self.project_type.name
        plot =plot_namex.upper()
        if 'DETACHED' in self.project_type.name:
            return self.env['report'].get_action(self.id, 'contruct_report.bh_city_offer_letter_report_document')
        elif 'TERRACE (KENSINGTON)' in self.project_type.name:
            return self.env['report'].get_action(self.id, 'contruct_report.report_offer_letter_new')

        elif 'FLAT' in self.project_type.name:
            return self.env['report'].get_action(self.id, 'contruct_report.galadimawa_carcas_offer_letter_report_document')
        elif 'MAISONETTE' in self.project_type.name:
            return self.env['report'].get_action(self.id, 'contruct_report.fish_letter_report_document_main')

        else:
            return self.env['report'].get_action(self.id, 'contruct_report.report_offer_letter_new')

    @api.multi
    def edit_letter_new(self):

        return self.env['report'].get_action(self.id, 'contruct_report.report_offer_letter_new')


    @api.multi
    def register_payment(self):

        return {
            'name': 'Register Payments',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'register.payment.wizard.for.plotx',
            'type': 'ir.actions.act_window',
            'target': 'new',
            'context': {
                'default_plot_id': self.id,
                'default_customer': self.partner_id.id,
                'default_ name_only_for_view': self.id,
                'default_customer_only_for_view': self.partner_id.id,
                #'default_ payment_view_id': payment_form_id,
                'default_amount':self.amount_dt,
                'default_project_name': self.name.name

            },
           }


    @api.multi
    def button_saleorder_create(self):
        record = self
        vals = {'name':self.building}
        product_id = self.env['product.product'].create(vals)
        vals1 = {'partner_id':self.partner_id}
        sale_order = self.env['sale.order'].create(vals1)
        #self.write({'so_no':sale_order.id})
        vals2 = {
                'order_id':sale_order,
                'product_id':product_id,
                'name':self.building,
                'price_unit':self.offer_price,
                'product_uom_qty':1,}
        sale_order_line_obj = self.env['sale.order.line']
        sale_order_line_obj.create(vals2)
        return {
            'name':_('Sale Order'),
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'sale.order',
            'type': 'ir.actions.act_window',
            'res_id': sale_order,
        }

    def calc_payment_plan(id, plot, offer_dt, payment_plan):
        intervals = 0
        offer_dt = datetime.strptime(offer_dt, '%Y-%m-%d')
        for count, interval in enumerate(self.env['account.payment.term.line'].search([('payment_id', '=', payment_plan.id)])):
            incr_interval += interval.days
            due_date = offer_dt + timedelta(days=incr_interval)
            value_amount = interval.value_amount
            interval_payment = interval.value
            if interval_payment == 'procent':  # procent ==> percent
                amount_to_pay = value_amount * plot.offer_price
            if interval_payment == 'fixed':
                amount_to_pay = value_amount
            values = {
                'project_and_plot_id': plot.id,
                'name': 'Payment ' + str(count + 1),
                'interval': str(interval.days) + ' days',
                'amount_to_pay': amount_to_pay,
                'due_date': due_date.strftime('%Y-%m-%d')
            }
            #self.env['plot.payment.breakdownx'].create(values)
            self.payment_breakdown.create(values)


    @api.multi
    def write(self, vals):
        plot = self.env['plot.allocatex'].browse(self.id)
        offer_dt = vals.get('offer_dt') or plot.offer_dt
        payment_plan = vals.get('payment_plan')
        payment_type = vals.get('payment_type')
        payment_plan_ids = self.env['plot.payment.breakdownx'].search([('project_and_plot_id', '=', self.id)])
        if payment_plan:
            plot = self.env['plot.allocatex'].browse(self.id)
            if payment_plan_ids:
                self.env['plot.payment.breakdownx'].browse(payment_plan_ids.ids).unlink()
                values = calc_payment_plan(plot.id, offer_dt, payment_plan)
            else:
                values = calc_payment_plan(off_pt.id, offer_dt, offer_price, payment_plan)
        if payment_type and payment_plan_ids and payment_type == 'outright':
            self.env['plot.payment.breakdownx'].browse(payment_plan_ids.ids).unlink()
            vals['payment_type'] = 'outright'
        val=super(plot_allocate,self).write(vals)
        return val



class project_project(models.Model):
    _inherit = "project.project"
    plot_ids = fields.Many2many('plot.allocatex', 'project_id',string = 'Plot')


class Account_Interface(models.Model):
    _name ='account.interfacex' #interface
    _rec_name ='journal_id'
    analytic_account = fields.Many2one('account.analytic.account', 'Analytic account')
    journal_id = fields.Many2one('account.journal', 'Account Journal', default=8)

class Register_Payment_View_Plot(models.Model):
    _name = 'register.payment.view.for.plotx'
    name = fields.Char('Name')
    plot_id = fields.Many2one('plot.allocatex', 'Payment For', required=True)
    customer = fields.Many2one('res.partner', 'Customer', required=True)
    payment_line = fields.One2many('register.payment.wizard.for.plotx', 'payment_view_id', string="Payments")
    @api.multi
    def print_payment_receipt(self):
        return self.env['report'].get_action(self.id, 'reports_module.total_payment_todate')


class Register_Payment_Wizard_For_Plot(models.Model):
    _name ='register.payment.wizard.for.plotx'
    payment_view_id = fields.Many2one('register.payment.view.for.plotx', string="Payment form")
    name = fields.Char('Name')
    plot_id=fields.Many2one('plot.allocatex', 'Payment For')
    customer=fields.Many2one('res.partner', 'Customer')
    payment_for = fields.Many2one('account.journal', 'Account Journals', required=True)
    bank = fields.Many2one('res.partner', 'Bank')#$$$$$$$$$$$$$$$$$$$ allocate
    #payment_for=fields.Many2one('account.interfacex', 'Account journal') #required=True) #$$$$$$$$$$$$$
    date=fields.Datetime('Date of payment', default=fields.Datetime.now)#, default= fields.datetime.now)
    amount = fields.Float('Paid Amount')
    payment_type = fields.Text('Narration')
    project_name = fields.Char('Project name')
    #advance_account = fields.Many2one('account.account', 'Advance Account', required=True)
    '''@api.multi
    def button_pay(self):

        print "##", "pay"
        return{'type': 'ir.actions.act_window_close'}'''
    @api.multi
    def button_pay(self):
        journal_obj = self.env['account.journal'].search([('id', '=', self.payment_for.id)])
        #payment_view_id = self.payment_view_id
        payment_view_data = {'name':'Payment for Building', 'plot_id':self.plot_id.id, 'customer':self.customer.id}
        account_voucher_data = {
            'partner_id': self.customer.id,
            'journal_id': self.payment_for.id,
            'type': 'receipt',
            'state': 'posted',
             'amount': self.amount,
            #'advance_account_id': vals.get('advance_account'),analytic_account
            'account_id': journal_obj.default_debit_account_id.id,
            #'reference': 'PF %s' %self.plot_id.plot_no.plot_no,
        }
        account_voucher_response = self.env['account.voucher'].create(account_voucher_data)
        #payment_view_id.create(payment_view_data)
        payment_view_form = self.env['register.payment.view.for.plotx'].create(payment_view_data)

        self.write({'payment_view_id':payment_view_form.id})

        self.env['plot.allocatex'].search([('id','=',self.plot_id.id)]).write({'payment_form_id':payment_view_form.id})
        payment_view_form2 = self.env['register.payment.view.for.plotx'].search([('id','=', payment_view_form.id)])
        valx = {
                'id':self.payment_view_id.id,
                'name':self.name,
                'customer':self.customer.id,
                'date':self.date,
                'amount':self.amount,
                'payment_for':self.payment_for.id,
                'payment_type': self.payment_type}
        payment_view_main = payment_view_form2.payment_line.payment_view_id.create(valx)


    @api.multi
    def print_receipt(self):
        return self.env['report'].get_action(self.id, 'reports_module.receipt_view_report')#construction_plot_4devnet.receipt_view_report


class Plot_Payment_Breakdown(models.Model):
    _name = 'plot.payment.breakdownx' # breakdown
    project_and_plot_id =fields.Many2one('plot.allocatex', 'Plot Number')
    name =fields.Char('Payment')
    interval =fields.Char('Payment Interval in days')
    amount_to_pay=fields.Float('Amount to be paid')
    due_date=fields.Datetime('Due date')

    @api.multi
    def print_receipt(self):
        return self.env['report'].get_action(self.id, 'reports_module.receipt_view_report')# allocate construction_plot_4devnet.receipt_view_report

class Title_Documents(models.Model):
    _name = 'title.documentsx'

    customer =fields.Many2one('res.partner', 'Customer', required=True)
    project_site= fields.Many2one('projectsite.masterx', 'Project Site', required=True)
    plot_no= fields.Many2one('unit.masterx', 'Plot Number', domain="[('status', '=', 'allocated')]", required=True)
    completed_documentation= fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Completed documentation')
    completed_payment=fields.Selection([('yes', 'Yes'), ('no', 'No')], 'Completed payment')
    vat_payment=fields.Selection([('yes', 'Yes'), ('no', 'No')], 'VAT Payment')

class Unit_Master(models.Model):
    _name = 'unit.masterx'
    _rec_name = 'plot_no'

    _sql_constraints = [
        ('combo', 'unique(project_site, plot_no, phase)', 'Error: Duplicate Project site, plot number and phase found'),
    ]

    project_site = fields.Many2one('projectsite.masterx', 'Project Site',required=True)
    building_type = fields.Many2one('buildingtype.masterx', 'Building Type', required=True)
    house_type = fields.Many2one('building.masterx', 'House Type', required=True)
    plot_no = fields.Char('Plot Number', required=True)
    offer_price = fields.Float('Offer Price', required=True)
    status= fields.Selection([('reserved', 'Reserved'),('unallocated', 'Unallocated'),('allocated', 'Allocated')],'Status', required=True, default ="unallocated" )
    phase = fields.Many2one('phase.masterx', 'Phase')#,default =1)
    block_no = fields.Many2one('block.masterx', 'Block No')
    investor = fields.Many2one('investor.masterx', 'Investor', required=True)
    analytic_account = fields.Many2one('project.project', domain="[('master_project', '=', False)]", string='Analytic account')

class ProjectSiteMaster(models.Model):
    _name = 'projectsite.masterx'
    name = fields.Char('Project Site', required = True)
    site_plan = fields.Binary('Site Plan')


class Buildingtype_Master(models.Model):
    _name = 'buildingtype.masterx'
    name = fields.Char('Type of Building', required = True)

class Building_Master(models.Model):
    _name = 'building.masterx'
    name = fields.Char('Building', required = True)

class Phase_Master(models.Model):
    _name = 'phase.masterx'
    name = fields.Char('Phase', required = True)

class Block_Master(models.Model):
    _name = 'block.masterx'
    name = fields.Char('Block No', required = True)

class Block_Master(models.Model):
    _name = 'investor.masterx'
    name = fields.Char('Investor', required = True)


class Plot_Deallocate(models.Model):
    _name = 'plot.deallocatex'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    plot_no = fields.Many2many('plot.allocatex', 'deallocate_and_plot', 'deallocate_id', 'plot_no', 'Select plot')
    reason = fields.Char('Reason', required=True)
    state = fields.Selection([
                    ('draft', 'Draft'),
                    ('approved', 'Approved')], 'Status', default='draft')

    @api.multi
    def unlink(self):
        for d in self:
            plot_no = d.plot_no.id
            plot_no.unlink()
        return True



    '''@api.multi
    def button_approve(self):


        plot_allocate = self.env['plot.allocatex'].search([('id','=', self.plot_no.id)])
        for plot in self.plot_no:
            plot_allocate.write({'state':'deallocation', 'active':False})
            unit_master_id = plot.plot_no.id
            unit_dae = self.env['product.product'].browse(unit_master_id) #unit_dae = self.env['unit.masterx'].search([('id', '=',unit_master_id)])
            unit_dae.write({'status':'unallocated'})
            body = "%s deallocated %s on %s" %(self.env.user.name, plot.plot_no.name,datetime.strftime(datetime.today(), '%D'))
            self.message_body(body=body)

        self.write({'state':'approved'})'''
    @api.multi
    def button_approve(self):
        plot_allocate_product = self.env['plot.allocatex']
        plot_allocate = self.env['plot.allocatex'].search([('id','=',self.plot_no.id)])
        for plot in self.plot_no:
            plot_allocate.write({'state':'deallocation', 'active':False})
            get_plot_no = self.env['product.product'].search([('id','=',plot.plot_no.id)])
            get_plot_no.write({'occupied_product':False})
            body = "%s deallocated %s" %(self.env.user.name, plot.plot_no.name)#datetime.now())
            self.message_post(body=body)



class Price_Review(models.Model):
    _name = "price.reviewx"
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'project_site'
    project_site = fields.Many2one('projectsite.masterx', 'Project Site',required=True)
    building_type = fields.Many2one('buildingtype.masterx', 'Building Type', required=True)
    house_type = fields.Many2one('building.masterx', 'House Type', required=True)
    current_price = fields.Many2one('Current Price')
    status = fields.Selection([('saved', 'Saved')], 'Status')
    phase = fields.Many2one('phase.masterx', 'Phase')
    investor =fields.Many2one('investor.masterx', 'Investor')

    @api.model
    def create(self, vals):
        project_site = vals.get('project_site')
        building_type = vals.get('building_type')
        house_type = vals.get('house_type')
        current_price = vals.get('current_price')
        plots= self.env['unit.masterx'].search(
            [
                '&',('project_site','=',project_site),('building_type','=',building_type),
                '&',('house_type','=',house_type), ('status', '=','unallocated')])
        vals['status']='saved'
        price_review_id = super(Price_Review, self).create(vals)
        for plot in plots:
            plot.offer_price = current_price

        return price_review_id



class Re_Sale(models.Model):
    _name = "re.salexx"
    name = fields.Many2one('projectsite.masterx','Project Site',required=True)
    plot_no = fields.Many2one('plot.allocatex','Plot Number',required=True,domain=[('state','=','allocation')], help="Plot no")
    source_cust= fields.Many2one('res.partner', 'Primary(Customer)',required=True)
    dest_cust= fields.Many2one('res.partner', 'Secondary(Customer)',required=True)
    purpose = fields.Char('Purpose')
    appl_deduct = fields.Selection([('yes','Yes'),('no','No')],'Applied Deduction(Yes/No)', default = 'yes')
    resale_fee= fields.Float('Resale Fee in %', default=1.00)
    amount_dt = fields.Float('Amount Paid todate')
    balance = fields.Float('Balance')

    @api.onchange('plot_no')
    def onchange_plot_id(self, vals):

        if self.plot_no:
            plot= self.env['plot.allocatex'].search([('id','=',self.plot_no.id)])
            vals= {}
            vals['source_cust'] = plot.partner_id.id
            vals['name'] = plot.name.id
            vals['amount_dt'] = plot.amount_dt
            vals['balance'] = plot.balance

    @api.model
    def create(self, vals):
        plot= self.env['plot.allocatex'].search([('id','=',self.plot_no.id)])
        plot_no = vals.get('plot_no')
        source_cust = vals.get('source_cust')
        dest_cust = vals.get('dest_cust')
        plot.write({'partner_id':dest_cust})
        return super(Re_Sale).create(vals)



class Plot_Revocate(models.Model):
    _name = 'plot.revocatex'
    name = fields.Many2one('projectsite.masterx','Project Site',required=True)
    plot_no = fields.Many2one('plot.allocatex','Plot Number',required=True,domain=[('state','!=','deallocation')])
    partner_id = fields.Many2one('res.partner', 'Customer',required=True)
    reason = fields.Char("Reason", required=True)
    amount_dt=fields.Float('Amount Paid')
    appl_deduct = fields.Selection([('yes','Yes'),('no','No')], 'Apply Deduction?(Yes/No)', default= 'yes')
    administrative_charge=fields.Float(string='Administrative Charge')
    refund_amount = fields.Float(string='Refund Amount', compute='cal_refund', store=True)
    #administrative_charge= fields.Float('Administrative charge in %', default= 10.00)
    #refund_amount =fields.Float('Refund Amount', compute ='cal_refund', store = True)
    allocation_stage= fields.Char('Allocated Stage')

    @api.depends('appl_deduct')
    def cal_refund(self):
        if self.appl_deduct == 'yes':
            refund = 0.0
            refund = (self.amount_dt - ((amount_dt * self.administrative_charge/100)))
            self.refund_amount = refund
        elif self.appl_deduct=='no':
            self.refund_amount= self.amount_dt

class Transfer(models.Model):
    _name = 'transferx'
    name = fields.Many2one('projectsite.masterx', 'Project Site', required=True)
    plot_no = fields.Many2one('plot.allocatex', 'Plot Number', required=True, domain=[('state','=','allocation')], help="Here Allocated Plot list only")
    source_cust = fields.Many2one('res.partner', 'Primary(Customer)', required=True)
    dest_cust = fields.Many2one('res.partner', 'Secondary(Customer)', required=True)
    purpose = fields.Char('Purpose', required=True)