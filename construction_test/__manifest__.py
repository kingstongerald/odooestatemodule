# -*- coding: utf-8 -*-
# See LICENSE file for full copyright and licensing details.

{
    'name': 'Construction Maduka',
    'version': '10.0.1.0.0',
    'author': 'Maduka Sopulu',
    'description':"""Construction""",
    'category': 'Construction',

    'depends': ['base','account','project'],
    'data': [
        'construction_view.xml',
        #'security/ir.model.access.csv'
    ],

    'installable': True,
    'auto_install': False,
}
